-- |
-- Author: Bladymir Tellez (tellezb@uci.edu)
-- Purpose: Sudoku Solver Puzzle
-- 
-- Acceptable Input: A single line with two space separated integers, that 
--                   reprresent the width and height of a single Block.
--                   The line is then followed by (p*q) lines of input with 
--                   p*q items accross each line, space separated.
-- 
--                    * See inputs Directory for examples.
-- 
-- This program will attempt to solve a provided Sudoku puzzle.
-- 
module Main
where 

import Data.List
import Data.Maybe
import Data.Char
import Control.DeepSeq
import Control.Applicative
import System.CPUTime
import System.Environment
import System.IO
import System.Timeout
import Text.Printf
import qualified Sudoku.IO                as SudokuIO
import qualified Sudoku.Strategies        as Strategies
import qualified Sudoku.Structures        as Structs
import qualified Sudoku.Helpers           as Helpers
import qualified Sudoku.GenericHelpers    as Generic
import qualified Data.Map                 as Map


solveMethodMap :: [String]
-- ^Available Methods for argument matching
-- These strings are used to match the provided parameters to the program.
solveMethodMap = ["BT", "FC", "MRV", "DH", "LCV", "ACP", "AC"]



solveMethods :: [Structs.SudokuBoard -> [Structs.SudokuBoard]]
-- ^List of method names that correspond their string counter-parts in position.
solveMethods = [Strategies.backtracking,
                Strategies.backtrackingFC,
                Strategies.backtrackingFC_MRV,
                Strategies.backtrackingFC_MRV_DH,
                Strategies.backtrackingFC_MRV_DH_LCV,
                Strategies.backtrackingFC_MRV_DH_LCV_ACP,
                Strategies.backtrackingFC_MRV_DH_LCV_AC]



timeOperation :: (a -> [Structs.SudokuBoard]) -> a -> IO Structs.SolutionTiming
-- ^Time a solve operation - take as parameter, a function that solved a puzzle an dreturn the timing of the puzzle.
timeOperation f a = do 
                       let res = take 1 $! [sol | sol <- f a, Helpers.isSolved sol]
                           solution = if null res then Structs.InvalidSudokuBoard else head res
                       start <- getCPUTime
                       end <- res `seq` getCPUTime
                       return $ Structs.SolutionTiming (fromIntegral (end - start) / (10^6) :: Double) solution



printToSpec :: Maybe Structs.SolutionTiming -> IO ()
-- ^Display the correct output depending on weather or not a timeout was encountered.
printToSpec x = 
  case x of 
    Nothing   -> 
      putStr $ "Time: >"++ show (60 * 10^6) ++"ms\nSolution: No\nTimeout: Yes\n"  
    (Just (Structs.SolutionTiming time board)) ->
        putStr $ "Time: "++ show time 
                  ++ "ms\nSolution: "
                  ++ (if board == Structs.InvalidSudokuBoard then "No" else "Yes") 
                  ++"\nTimeout: No\n"



maxTime :: Int
-- ^Timeout, default is 60 seconds.
maxTime = Generic.toMicro 60



main :: IO ()
-- ^Application Entry Point
main = do
       args <- getArgs
       let methodIndex = if null args
                            then 0
                            else fromMaybe 0 $ elemIndex (map toUpper . head $ args) solveMethodMap

       boardSize <- getLine
       board     <- getContents
       let size = SudokuIO.readBoardSize boardSize
       let n = head size
       let p = head . drop 1 $ size
       let q = head . drop 2 $ size
       start <- getCPUTime
       let sudokuBoard = SudokuIO.mkSudokuFromInput (SudokuIO.readBoardInput board) (SudokuIO.generateEmptyBoard p q)
       let solveMethod = solveMethods !! methodIndex
       printToSpec =<< timeout maxTime (timeOperation solveMethod sudokuBoard)
       
       putStr "\n\n"


