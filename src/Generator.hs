------------------------------------------------------------------------------
-- | Author: Bladymir Tellez (tellezb@uci.edu)
-- | Purpose: Sudoku Puzzle Generator
-- | 
-- | Acceptable Input: A single line with 3 space separated integers, that
-- |                   represent the length of a single side of the puzzle (N)
-- |                   the size of a single row in a block (p), the size of a 
-- |                   single column in a block (q), and the number of prefilled
-- |                   in squares to generate, in the following format
-- |                   
-- |                   N p q M
-- | 
-- | 
-- | This program will attempt to generate a sudoku puzzle of size NxN with M
-- | prefilled in squares.
------------------------------------------------------------------------------
module Main
where
import Control.Monad
import Control.Applicative
import System.CPUTime
import System.IO
import System.Random
import Data.List
import Data.Char
import Data.Maybe
import Data.Tuple
import qualified Sudoku.IO         as SudokuIO
import qualified Sudoku.Strategies as Strategies
import qualified Sudoku.Structures as Structs
import qualified Sudoku.Helpers    as Helpers
import qualified Data.Map          as Map
import qualified Data.Set          as Set

-- |Constant definition holding the prompt for parameters.
promptInput :: String
promptInput = "Please Enter The Parameters in the format: N p q M\n"

-- |Constant defenition holding the error message for wrong input.
promptTryAgain :: String
promptTryAgain = "Invalid Input, Please Try Again\n"


-- | Generator Application Entry Point.
main :: IO ()
main = 
  do 
     npqm <- getArguments
     let sudokuBoard =  SudokuIO.generateEmptyBoard (Structs.getP npqm) (Structs.getQ npqm)
     putStr $ show (Structs.getN npqm) ++ " " ++ show (Structs.getP npqm) ++ " " ++ show (Structs.getQ npqm) ++ "\n"
     solution <- generateValidBoard npqm sudokuBoard
     print solution



-- |Generate a board that follows the rules of the sudoku game. If the generated puzzle does
-- not follow the rules of the game then try again..and again...and again....
generateValidBoard :: Structs.GeneratorArgs -> Structs.SudokuBoard -> IO Structs.SudokuBoard
generateValidBoard args sudokuBoard = 
  do 
     -- Generate a board with the given M, and a starting blank sudokuBoard.
     -- assignValuesToCoords will choose cells randomly, and then assign the valus randomly.
     generated <- assignValuesToCoords (Structs.getM args) sudokuBoard
     if Helpers.isValidBoard generated 
       -- The board generated is a valid board, so we have found one!
       then return generated
       -- The board we generated is not a valid board, so generate another one!
       else generateValidBoard args sudokuBoard



-- |Retreives the arguments from STDIN, and does not return until 
-- a valid set of arguments are provided.
getArguments :: IO Structs.GeneratorArgs
getArguments = 
  do
     --putStr promptInput
     args <- words <$> getLine
     if isValidArgumentList args
         then return $ (\(n:p:q:m:_) -> Structs.GeneratorArgs n p q m) (map read args :: [Int])
         else do
                 putStr promptTryAgain
                 getArguments



-- |Determine if the provided arguments are valid according to a given criteria.
-- All 4 arguments are Integers
-- N == p * q
-- M <= N^2
isValidArgumentList :: [String] -> Bool
isValidArgumentList input = 
  let correctSize = (==) 4 . length
      areInts     = and . concatMap (map isDigit)
      correctRanges = ((read $ input !! 0 :: Int) == ((read $ input !! 1 :: Int) * (read $ input !! 2 :: Int)))
                      && ((read $ input !! 3 :: Int) <= (read $ input !! 0 :: Int)^2)
  in (length input == 4) && (correctSize input && areInts input && correctRanges)



-- |Choose M coordinates randomly from a list of available coordinates.
generateXCoords :: Int -> [Structs.Coordinates] -> IO ([Structs.Coordinates], [Structs.Coordinates])
generateXCoords n available = foldl (>>=) (return ([], available)) (replicate n randomCoord)



-- |Get a random Coordinate from a list of coordinate, and return the updated list with out that given coordinate.
randomCoord :: ([Structs.Coordinates],[Structs.Coordinates]) -> IO ([Structs.Coordinates], [Structs.Coordinates])
randomCoord (rxs, axs) = 
  do
     x <- randomRIO (0, max 0 (length axs - 1))
     let new     = axs !! x
         updated = filter (/= new) axs
     return (new:rxs, updated)



-- |Assign a given number of values to the provided sudoku board.
assignValuesToCoords :: Int -> Structs.SudokuBoard -> IO Structs.SudokuBoard
assignValuesToCoords n board = 
  do 
     let width = Structs.getNumRows board * Structs.getNumColumns board
     coords <- fst <$> generateXCoords n [(x, y) | x <- [1..width], y <- [1..width]] -- (Map.keys (Structs.getMapping board))
     let foldit brd coord = do 
                               b <- brd
                               chooseValueFor b coord
     foldl' foldit (pure board) coords

                                     


-- |Choose a value for the provided sudoku board for the provided coordinate
chooseValueFor :: Structs.SudokuBoard -> Structs.Coordinates -> IO Structs.SudokuBoard
chooseValueFor board@(Structs.SudokuBoard p q mapping) coord = 
  let domain = Helpers.getCellDomain (Map.lookup coord mapping)
  in  if null domain
      then  return board -- error "EMPTY DOMAIN" 
      else  do 
            nVal <- chooseRandomDomainValue domain
            return $ Helpers.setCellValue nVal coord (Structs.Cell (0, Set.empty)) $ Strategies.doForwardChecking board coord nVal
                                                          


-- |Randomly choose a value from the list of available cell values
chooseRandomDomainValue :: [Structs.CellValue] -> IO Int
chooseRandomDomainValue available = 
  do 
     i <- randomRIO (0, max 0 (length available - 1))
     return (available !! i)
