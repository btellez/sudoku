------------------------------------------------------------------------------
-- | Author: Bladymir Tellez (tellezb@uci.edu)
-- | Purpose: Sudoku Solver Puzzle
-- | 
-- | Sudoku.Helpers contains functions that that help faciliate the access of
-- | certain data strucrture members or substructures.
------------------------------------------------------------------------------

module Sudoku.Helpers
where
import Data.List
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Sudoku.Structures as Structs



getRow :: Int -> Structs.SudokuBoard -> [((Structs.RowCoord, Structs.ColCoord), Structs.Cell)]
-- ^Helper Method to Return a Given Row
getRow idx (Structs.SudokuBoard p q mapping) = map (\coord -> case Map.lookup coord mapping of Just a -> (coord, a)) range
    where range = zip (replicate top idx) [1..top]
          top = p * q



getCol :: Int -> Structs.SudokuBoard -> [((Structs.RowCoord, Structs.ColCoord), Structs.Cell)]
-- ^Helper Method to Return a given Column
getCol idx (Structs.SudokuBoard p q mapping) = map (\coord -> case Map.lookup coord mapping of Just a -> (coord, a)) range
    where range = zip [1..top] $ replicate top idx
          top = p * q



getBlock :: Int -> Structs.SudokuBoard -> [((Structs.RowCoord, Structs.ColCoord), Structs.Cell)]
-- ^Helper Method to Return a given Block from the Sudoku Board
getBlock idx (Structs.SudokuBoard p q mapping) = map (\c -> case Map.lookup c mapping of Just v -> (c, v)) range
    where range = [(x,y) | x <- coordsX, y <- coordsY]
          --maxX = p * (idx - 1 `mod` q + 1)
          maxX = (1 + ((idx-1) `div` p)) * p
          coordsX = [(maxX - p + 1)..maxX]
          maxY = q * (((idx - 1) `mod` p) + 1)
          coordsY = [(maxY - q + 1)..maxY]
          


getRowNum :: Structs.Coordinates -> Int
-- ^Method returns the index of the row that the coordinate belongs to.
getRowNum (x,_) = x



getColNum :: Structs.Coordinates -> Int
-- ^Method returns the index of the column that the coordinate belongs to.
getColNum (_,y) = y



getBlockNum :: Int -> Int -> Structs.Coordinates -> Int
-- ^Method returns the index of the block strucutre that the given coordinate belongs to.
getBlockNum p q (x,y) = case x > maxim || y > maxim of
                        True      -> error "Index is out of bounds! WTF?"
                        otherwise -> (((x - 1) `div` p) * p) + ((y - 1) `div` q) + 1
                        where maxim = q * p


updateCellAt :: Structs.SudokuBoard -> Structs.Coordinates -> Structs.Cell -> Structs.SudokuBoard
-- ^Updates the mapping with a new cell
updateCellAt (Structs.SudokuBoard p q mapping) coord cell = Structs.SudokuBoard p q (Map.insert coord cell mapping)



updateCellValueAt :: Structs.Coordinates -> Structs.CellValue -> Structs.SudokuBoard -> Structs.SudokuBoard
-- ^Method Updates the Value of a given coordinate to a given value and sets the domain to an empty set.
updateCellValueAt coord newValue (Structs.SudokuBoard p q mapping) = case Map.lookup coord mapping of 
                                                                          Nothing  -> Structs.SudokuBoard p q mapping  -- Nothing to update
                                                                          Just _   -> Structs.SudokuBoard p q $ Map.insert coord (Structs.Cell (newValue, Set.empty)) mapping



getConstraintList :: Structs.Coordinates -> Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)]
-- ^Returns a list of Block, Column and Row's that participate in the constaints with a given coordinate.
getConstraintList coords board = filter nonempty $ foldr (:) (foldr (:) (foldr (:) [] block) column) row -- block ++ column ++ row
                               where block   = getBlock (getBlockNum (Structs.getNumRows board) (Structs.getNumColumns board) coords) board
                                     column  = getCol (getColNum coords) board
                                     row     = getRow (getRowNum coords) board
                                     nonempty (_, Structs.Cell (_, domain)) = not $ Set.null domain



updateCellDomainAt :: Structs.Coordinates -> Structs.CellValue -> Structs.SudokuBoard -> Structs.SudokuBoard
-- ^Method updates the domain of the cell at the given coordinates by removing the value provided.
updateCellDomainAt coord val (Structs.SudokuBoard p q mapping) = case Map.lookup coord mapping of 
                                                                      Nothing -> Structs.SudokuBoard p q mapping -- nothing to update
                                                                      Just (Structs.Cell (cval, domain))  -> Structs.SudokuBoard p q (Map.insert coord (Structs.Cell (cval, Set.delete val domain)) mapping)



updateUnitDomain :: Structs.SudokuBoard -> Structs.CellValue -> [(Structs.Coordinates, Structs.Cell)] -> Structs.SudokuBoard
-- ^Method Updates the Domain for all the other cells in a given unit.
updateUnitDomain board x = foldl' domUpdate board
                            where domUpdate brd (coord,  _) = updateCellDomainAt coord x brd



getUnassignedList :: Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)]
-- ^Method Returns the list of Cells that are unassigned.
getUnassignedList (Structs.SudokuBoard p q mapping) = Map.toList $ Map.filter unassigned mapping
                                                    where unassigned (Structs.Cell (val, dom)) = val == 0



getUnassignedSingleDomainList :: Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)]
-- ^Get the unsigned cells that have only a single value left in their domains.
getUnassignedSingleDomainList board = filter singleDomain $ getUnassignedList board
                                    where singleDomain (_, Structs.Cell (val, dom)) = Set.size dom == 1



getAssignedList :: Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)]
-- ^Method Returns the list of Cells that are unassigned.
getAssignedList (Structs.SudokuBoard p q mapping) = Map.toList $ Map.filter unassigned mapping
                                                    where unassigned (Structs.Cell (val, dom)) = val /= 0



getCellDomainString :: Maybe Structs.Cell -> String
-- ^Return a string of the Cell's domain.
getCellDomainString Nothing = ""
getCellDomainString (Just (Structs.Cell (val, domain))) = show val ++ " : " ++ show domain



getCellDomain :: Maybe Structs.Cell -> [Int]
-- ^Return a list of the Cell's domain.
getCellDomain Nothing = []
getCellDomain (Just (Structs.Cell (_, domain))) = Set.toList domain



updateSingleDomainCells :: Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)] -> Structs.SudokuBoard
-- ^Update the cells with a single domains to have the only value in the domain be assigned as the value of the cell.
updateSingleDomainCells (Structs.SudokuBoard p q mapping) xs = Structs.SudokuBoard p q (foldl' f mapping xs)
                                                             where f mapping (coord, Structs.Cell (val, domain)) = Map.insert coord (Structs.Cell (head (Set.elems domain), domain)) mapping



setCellValue :: Structs.CellValue -> Structs.Coordinates -> Structs.Cell -> Structs.SudokuBoard -> Structs.SudokuBoard
-- ^Alias to updateCellAt
setCellValue nVal coords _ board = updateCellAt board coords (Structs.Cell (nVal, Set.insert nVal Set.empty))



getValues :: [(Structs.Coordinates, Structs.Cell)] -> [Structs.CellValue]
-- ^Returns a list of just the values from a given set of constraints.
-- Useed for comparing a set of constraints to a set of known values. eg [1..9] == sort getValues (...)
getValues ((_, Structs.Cell(val, domain)):[]) = [val]
getValues ((_, Structs.Cell(val, domain)):xs) = val : getValues xs



isValidBoard :: Structs.SudokuBoard -> Bool
-- ^Determine if the board is valid by identifying if any of the cells contain an empty domain.
isValidBoard board = let unassignedCells = getUnassignedList board
                         emptyDomains (_, Structs.Cell (_, dom)) = Set.null dom
                     -- It is true that the list of cells with empty domains is empty, this making this a valid board.
                     in not . any emptyDomains $ unassignedCells



isConsistent :: Structs.SudokuBoard -> Structs.Coordinates -> Structs.CellValue -> Bool
-- ^Tell's us wheather an assignment to a given coordinate would result in a consistent board.
-- This keeps us from exploring inconsistant (rule violating) states.
isConsistent  board srcCoords value = not . any (\(clCo, Structs.Cell (clVal, _)) -> value == clVal) $ getConstraintList srcCoords board



isSolved :: Structs.SudokuBoard -> Bool
-- ^Function tells us wheather or not a given sudoku board has been solved.
isSolved Structs.InvalidSudokuBoard = False
isSolved (Structs.SudokuBoard p q mapping) = foldl' consistentCell True (Map.toList mapping)
              where consistentCell acc (coord, cell) =  acc && ([1..range] == (sort . getValues . getBlock (getBlockNum p q coord) $ Structs.SudokuBoard p q mapping))
                    getValue (Structs.Cell (value, domain)) = value
                    range = p * q
