-- |Author: Bladymir Tellez (tellezb@uci.edu)
-- Purpose: Sudoku Solver Puzzle
-- 
-- Sudoku.Structures module is the module concerned with the defining of 
-- useful data structures and their behavior to be able to manage program 
-- data.
module Sudoku.Structures
where
import Text.Printf
import Data.Char
import Sudoku.GenericHelpers
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.List as List

-- |Alias for an Int type
type CellValue = Int
-- |Alias for an Int type
type RowCoord  = Int
-- |Alias for an Int type
type ColCoord  = Int
-- |Alias for an Int type
type RowCount  = Int
-- |Alias for an Int type
type ColCount  = Int
-- |Alias for an Int type
type Coordinates = (RowCoord, ColCoord)

-- |Deleting Set is set of Coordinate-Value which had previously been removed from a Domain.
type DeletionSet = [(Coordinates, CellValue)]

-- |Stores a coordinate and cell value after reading input
type CoordinateValueList = [(Coordinates, CellValue)]

-- |This data Structure stores the domain of values that a particular variable can take
type Domain = Set.Set Int

-- |Cell represents a single cell in the board.
data Cell = Cell (CellValue, Domain)

-- |Mapping is the storage mechanism for the cells 
type Mapping = Map.Map Coordinates Cell

-- |Encapsulates the arguments for use in a generator.
data GeneratorArgs = GeneratorArgs {getN :: Int, getP :: Int, getQ :: Int, getM :: Int} deriving (Show)

-- |This structure stores the size of our board, alodng with the mapping of our values.
data SudokuBoard = InvalidSudokuBoard | SudokuBoard { getNumRows :: Int, getNumColumns :: Int, getMapping :: Mapping }

-- |Encapsulate the solution and the timing to be able to define the behaviour of show
data SolutionTiming = SolutionTiming {getTime :: Double, getSolution :: SudokuBoard}

-- |Custom Instance for Custom Variable Types and the show function.
instance Show Cell where
    show (Cell (value, domain)) = show value ++ " D: "++ show (Set.toList domain)

-- |Define the way to display the sudoku board
instance Show SudokuBoard where
    show (SudokuBoard p q mapping) = unlines $ map (concatMap (++" ")) (groupBy row list)
                                     where groupBy y [] = []
                                           groupBy y xs = take y xs : groupBy y (drop row xs)
                                           row = p * q
                                           list = map disp $ Map.toList mapping
                                           disp (coords, Cell (val, domain)) = if val /= 0 then toBase35 val else "0"
    show (InvalidSudokuBoard)    = "Invalid Sudoku Board!"

instance Eq SudokuBoard where
  InvalidSudokuBoard == InvalidSudokuBoard = True
  InvalidSudokuBoard == _ = False
  _ == InvalidSudokuBoard = False
  _ == _ = True

instance Show SolutionTiming where
    show (SolutionTiming time board) = "Time Taken : " ++ printf "%0.3f" time ++ "\n\nSolution:\n" ++ show board



