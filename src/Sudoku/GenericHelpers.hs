module Sudoku.GenericHelpers 
( base35,
  fromBase35,
  toBase35,
  toMicro)
where
import Data.Char
import qualified Data.List as List



base35 :: String
-- ^Generate the list of charactes that are valid in our numbering system
base35 = [x | x <- ['1'..'Z'], isAlphaNum x]



toBase35 :: Int -> String
-- ^Helper method to conver into base35 strings.
toBase35 n = let base = 35 
             in if n <= base
                  then [base35 !! ((n - 1) `mod` base)]
                  else (base35 !! ((n `div` base) - 1)) : [base35 !! ((n - 1) `mod` base)]



fromBase35 :: String -> Int
-- ^Helper method to read in base35 strings.
fromBase35 n = let mult p a = a * 35^p  
                   elemIdx x = case List.elemIndex (toUpper x) base35 of
                                  Nothing -> 0
                                  Just i  -> i + 1 
               in sum . zipWith mult [0..] . reverse . map elemIdx $ n


timeoutSecondsBound :: Int
-- ^Maximum allowed number of seconds, before triggering an Int out of boudns exception.
timeoutSecondsBound = 2747


toMicro :: Int -> Int
-- ^Convert from Seconds to Micro-Seconds
toMicro n 
    | n < 0     = 0
    | otherwise = if n > timeoutSecondsBound
                    then error "Timeout is too large, must be less than 2747!"
                    else n * (10^6)
