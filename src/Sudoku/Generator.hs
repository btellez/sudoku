------------------------------------------------------------------------------
-- | Author: Bladymir Tellez (tellezb@uci.edu)
-- | Purpose: Sudoku Solver Puzzle
-- | 
-- | Sudoku.Generator contains code for generating a sudoku puzzle.
------------------------------------------------------------------------------

module Sudoku.Generator
where
import Data.List
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Sudoku.Structures as Structs
import qualified Sudoku.Helpers    as Helpers
import qualified Sudoku.Strategies as Strategies