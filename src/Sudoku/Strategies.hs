------------------------------------------------------------------------------
-- | Author: Bladymir Tellez (tellezb@uci.edu)
-- | Purpose: Sudoku Solver Puzzle
-- | 
-- | Sudoku.Strategies module is the module concerned with the defining of
-- | useful strategies for solving a Sudoku puzzle. This contains techniques
-- | that utilize the Sudoku.Structures and operations on them.
------------------------------------------------------------------------------

module Sudoku.Strategies
where
import           Data.List
import           Control.Monad
import qualified Data.Map          as Map
import qualified Data.Set          as Set
import qualified Sudoku.Structures as Structs
import qualified Sudoku.Helpers    as Helpers



doForwardChecking :: Structs.SudokuBoard -> Structs.Coordinates -> Structs.CellValue -> Structs.SudokuBoard
-- ^Simple Forward Checking, does the propagation of values to domains of other values.
-- Input: Sudoku Boars, Coordinates of Original Node, and Calue to propagate.
-- Return: Sudoku Board configuration with the updated configuration
doForwardChecking board srcCoords value = foldl' foldit board updatedConstraintList
                                     where constraints = Helpers.getConstraintList srcCoords board
                                           removeFromDomain val (coords, Structs.Cell (cellval, domain)) = if srcCoords /= coords
                                                                                                            then (coords, Structs.Cell (cellval, Set.delete val domain)) 
                                                                                                            else (coords, Structs.Cell (cellval, domain))
                                           updatedConstraintList = map (removeFromDomain value) constraints
                                           foldit sb (coord, cell) = Helpers.updateCellAt sb coord cell



maintainArcConsistency :: Structs.SudokuBoard -> Int -> Structs.SudokuBoard
-- ^Takes Assigned cells, and propagates the assignments to the constraints
-- until there are no more assigned nodes to propagate.
maintainArcConsistency board tier
        | tier == 0 = maintainArcConsistency (foldl' forwardChecking board (tier0 board)) (tier+1)
        | otherwise = if null $ tierN board 
                        then board
                        else maintainArcConsistency  (foldl' forwardChecking updateSingles (tier0 updateSingles)) (tier+1)
        where tier0 = Helpers.getAssignedList
              tierN = Helpers.getUnassignedSingleDomainList
              updateSingles = Helpers.updateSingleDomainCells board (tierN board)
              forwardChecking sb (coord, Structs.Cell (v, d)) = doForwardChecking sb coord v



backtracking :: Structs.SudokuBoard -> [Structs.SudokuBoard]
-- ^Backtracking search (BT) only.
-- Simple Backtracking on a SudokuBoard
-- Values are assigned from top to bottom, left to right
backtracking board = case Helpers.getUnassignedList board of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do
                                                                  pVal <- Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtracking (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) board)
                                                                    else return Structs.InvalidSudokuBoard



backtrackingFC :: Structs.SudokuBoard -> [Structs.SudokuBoard]
-- ^BT plus Forward Checking (FC) for limited constraint propagation.
-- Backtracking with Forward Checking on a Sudoku Board
-- Values are assigned from top to bottom, left to right
-- All value selection are forward checked after selection
backtrackingFC board = case Helpers.getUnassignedList board of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do 
                                                                  pVal <- Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtrackingFC (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) (doForwardChecking board coords pVal))
                                                                    else return Structs.InvalidSudokuBoard



backtrackingFC_MRV :: Structs.SudokuBoard -> [Structs.SudokuBoard]
-- ^BT+FC plus Minimum Remaining Values (MRV) for variable ordering. 
-- Backtracking with Forward Checking on a Sudoku Board, and MRV ordering
-- Values are assigned from top to bottom, left to right
-- All value selection are forward checked after selection
backtrackingFC_MRV board = case mrvOrder (Helpers.getUnassignedList board) of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do 
                                                                  pVal <- Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtrackingFC (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) (doForwardChecking board coords pVal))
                                                                    else return Structs.InvalidSudokuBoard



mrvOrder :: [(Structs.Coordinates, Structs.Cell)] -> [(Structs.Coordinates, Structs.Cell)]
-- ^Take a set of coordiantes and cell structures and sort by the minimum remaining values.
mrvOrder = sortBy remainingValues
            where remainingValues (_,Structs.Cell (_,dA)) (_,Structs.Cell (_,dB)) = compare (Set.size dA) (Set.size dB)



backtrackingFC_MRV_DH :: Structs.SudokuBoard -> [Structs.SudokuBoard]
-- ^BT+FC+MRV plus Degree Heuristic (DH) to break ties for variable ordering.
-- Backtracking with Forward Checking on a Sudoku Board, and MRV ordering
-- Values are assigned from top to bottom, left to right
-- All value selection are forward checked after selection
backtrackingFC_MRV_DH board = case mrvOrder_Degree (Helpers.getUnassignedList board) board of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do 
                                                                  pVal <- Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtrackingFC (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) (doForwardChecking board coords pVal))
                                                                    else return Structs.InvalidSudokuBoard


mrvOrder_Degree :: [(Structs.Coordinates, Structs.Cell)] -> Structs.SudokuBoard -> [(Structs.Coordinates, Structs.Cell)]
-- ^Take a set of coordinates and cell-srtuctures and sorty by the number of remaining values, and then compare by the degrees of participation
-- if the cell ties in number of minimum values with another cell.
mrvOrder_Degree xs board = sortBy remainingValues xs
                           where remainingValues (cA, Structs.Cell (vA,dA)) (cB, Structs.Cell (vB,dB)) = case compare (Set.size dA) (Set.size dB) of
                                                                                                              EQ    -> compareDegree cA cB board
                                                                                                              GT    -> GT
                                                                                                              LT    -> LT



compareDegree :: Structs.Coordinates -> Structs.Coordinates -> Structs.SudokuBoard -> Ordering
-- ^Take two coordinates, and compare the degree of participation of each of the cells. and return result of `compare coordA cordB`
compareDegree coordA coordB board = compare (length (Helpers.getConstraintList coordB board)) (length (Helpers.getConstraintList coordA board))



backtrackingFC_MRV_DH_LCV :: Structs.SudokuBoard -> [Structs.SudokuBoard]
-- ^BT+FC+MRV+DH plus Least Constraining Value (LCV) for value ordering.
-- Backtracking with Forward Checking on a Sudoku Board, and MRV ordering
-- Values are assigned from top to bottom, left to right
-- All value selection are forward checked after selection
backtrackingFC_MRV_DH_LCV board = case mrvOrder_Degree (Helpers.getUnassignedList board) board of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do 
                                                                  pVal <- lcvOrdering board coords $ Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtrackingFC (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) (doForwardChecking board coords pVal))
                                                                    else return Structs.InvalidSudokuBoard



lcvOrdering :: Structs.SudokuBoard -> Structs.Coordinates -> [Structs.CellValue] -> [Structs.CellValue]
-- ^Find the Least Constrained value ordering.
lcvOrdering board coords xs = map fst $ sortBy (\(x1,y1) (x2,y2) -> compare y1 y2) 
                                               (Map.toList $ foldl countem (Map.fromList $ zip xs [0..]) 
                                                                            (Helpers.getConstraintList coords board))


-- |Helper function used in lcvOrdering. The function counts the number of times the value in a domain occurs.
countem (mapping) (coords, Structs.Cell (val, domain)) = foldl fx mapping $ Set.toList domain
                                                         where fx xmap d = case Map.lookup d xmap of
                                                                   Nothing -> xmap
                                                                   Just cnt -> Map.insert d (cnt+1) xmap



-- |BT+ FC+MRV+DH+LCV plus Arc Consistency as a pre-processing step only (ACP). 
backtrackingFC_MRV_DH_LCV_ACP :: Structs.SudokuBoard -> [Structs.SudokuBoard]
backtrackingFC_MRV_DH_LCV_ACP board = backtrackingFC_MRV_DH_LCV (maintainArcConsistency board 0)



-- |BT+ MRV+DH+LCV+ACP plus Arc Consistency after each step (AC).
-- Do the prep-processing AC step, then move on to doing AC after every step(ACR).
backtrackingFC_MRV_DH_LCV_AC :: Structs.SudokuBoard -> [Structs.SudokuBoard]
backtrackingFC_MRV_DH_LCV_AC board = backtrackingFC_MRV_DH_LCV_ACR $ maintainArcConsistency board 0


-- |BT+ MRV+DH+LCV+ACP plus Arc Consistency after each step (AC).
backtrackingFC_MRV_DH_LCV_ACR :: Structs.SudokuBoard -> [Structs.SudokuBoard]
backtrackingFC_MRV_DH_LCV_ACR board = case mrvOrder_Degree (Helpers.getUnassignedList board) board of
                      []                                       -> return board
                      ((coords, Structs.Cell (val, domain)):_) -> do 
                                                                  --pVal <- Set.toList domain
                                                                  pVal <- lcvOrdering board coords $ Set.toList domain
                                                                  if Helpers.isConsistent board coords pVal
                                                                    then backtrackingFC (maintainArcConsistency (Helpers.setCellValue pVal coords (Structs.Cell (val, domain)) $! doForwardChecking board coords pVal) 0)
                                                                    else return board



-- |BT+FC+ACP plus Arc Consistency as a pre-processing step only (ACP). 
backtrackingFC_ACP :: Structs.SudokuBoard -> [Structs.SudokuBoard]
backtrackingFC_ACP board = backtrackingFC (maintainArcConsistency board 0)
