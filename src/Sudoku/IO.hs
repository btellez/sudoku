------------------------------------------------------------------------------
-- | Author: Bladymir Tellez (tellezb@uci.edu)
-- | Purpose: Sudoku Solver Puzzle
-- | 
-- | Sudoku.IO module is the module concerned with the reading of input
-- | and conversion to a managable datastructure from data fed into STDIN.
------------------------------------------------------------------------------

module Sudoku.IO
where 
import qualified Data.Map          as Map
import qualified Data.Set          as Set
import qualified Sudoku.Structures as Structs
import qualified Sudoku.GenericHelpers as Generic



readBoardSize :: String -> [Int]
-- ^Read the board size.
readBoardSize = map read . words


                   
readBoardInput :: String -> [[Int]]
-- ^Convert the Provided Strings into useable inputs.
readBoardInput io = map conversion (buildBoardLayout io)
                    where buildBoardLayout = map words . lines
                          conversion = map Generic.fromBase35



mkSudokuFromInput :: [[Int]] -> Structs.SudokuBoard -> Structs.SudokuBoard
-- ^Method Creates a Sudoku Board with the appropriate Values and domains. 
mkSudokuFromInput io (Structs.SudokuBoard row col mapping) = Structs.SudokuBoard row col (foldl insertRow mapping values)
                                                            where values = associateValuesToCoordinates io
                                                                  insertRow = foldl insertCell
                                                                  insertCell themap (coord, cell) = Map.insert coord cell themap



associateValuesToCoordinates :: [[Int]] -> [[(Structs.Coordinates, Structs.Cell)]]
-- ^Method associates a given input to corresponding coordinates
associateValuesToCoordinates xsxs = map enumerateCells $ enumerateRows xsxs
                            where enumerateRows = zip [1..]
                                  enumerateCells (row, xs) = zip (coords row xs) (mkCellsFromList xs)
                                  coords row xs = zip (repeat row) [1..length xs]
                                  mkCellsFromList xs = [Structs.Cell (val, domain) | val <- xs, let domain = Set.fromList $ if val == 0 then [1..length xs] else [val]] 



generateEmptyBoard :: Structs.RowCount -> Structs.ColCount -> Structs.SudokuBoard
-- ^Method generates an empty board with all values set to zero and all domains to 1 to range.
generateEmptyBoard p q = Structs.SudokuBoard p q $ Map.fromList $ zip coordinates emptyCells
                       where emptyCells = replicate (range * range) $ Structs.Cell (0, Set.fromList [1..p*q])
                             coordinates = [(x,y) | x <- [1..range], y <-[1..range]]
                             range = p * q
