module Main
where 
import System.CPUTime
import Text.Printf
import Data.List
import Data.Maybe
import Data.Monoid
import Control.DeepSeq
import Control.Parallel.Strategies
import Control.Parallel
import Control.Monad.Trans.State
import Control.Monad.Writer
import System.Timeout
import qualified Sudoku.IO                as SudokuIO
import qualified Sudoku.Strategies        as Strategies
import qualified Sudoku.Structures        as Structs
import qualified Sudoku.Helpers           as Helpers
import qualified Sudoku.GenericHelpers    as Generic
import qualified Data.Map                 as Map  

--type StatefulBoard = State (Sum Int) Structs.SudokuBoard
type LogBoard = Writer (Sum Int) Structs.SudokuBoard

assign board x = writer (Helpers.updateCellValueAt (1,x) x board, Sum 1)


makeAssignment :: Int -> Structs.SudokuBoard -> LogBoard
makeAssignment x board = assign board x
                      

main :: IO ()
main = do 
          putStrLn "\nStatefull Board Computation:\n"
          let board = SudokuIO.generateEmptyBoard 3 3
          print $ runWriter $ makeAssignment 1 board
