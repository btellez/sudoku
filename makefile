build:
	ghc --make -o Main Main

generator:
	ghc --make -o Generator Generator

stateful:
	ghc --make -o Stateful Stateful

solver:
	ghc --make -threaded Solver

build-profile:
	ghc -O2 --make -threaded Main.hs -prof -auto-all -caf-all -fforce-recomp -rtsopts

profile12:
	cat inputs/12x12-1.in | ./Main 1e6 +RTS -i0.001 -hc -p -K100M

clean:
	rm *.o *.hi && rm Sudoku/*.o Sudoku/*.hi

testgen:
	echo "12 3 4 3" | ./Generator

test:
	cat inputs/9x9-2.in | ./Main

test1:
	cat inputs/9x9-1.in | ./Main

test2:
	cat inputs/9x9-2.in | ./Main

test3:
	cat inputs/9x9-3.in | ./Main

test4:
	cat inputs/9x9-4.in | ./Main

testhard:
	cat inputs/9x9-hard.in | ./Main

test12:
	cat inputs/12x12-1.in | ./Main

test12-2:
	cat inputs/12x12-2.in | ./Main

test12-3:
	cat inputs/12x12-3.in | ./Main

test12-easy1:
	cat inputs/12x12-easy1.in | ./Main

test16:
	cat inputs/16x16-1.in | ./Main

test16-1:
	cat inputs/16x16-1.in | ./Main

test16-2:
	cat inputs/16x16-2.in | ./Main

test16-3:
	cat inputs/16x16-3.in | ./Mai

test16-4:
	cat inputs/16x16-4.in | ./Main
