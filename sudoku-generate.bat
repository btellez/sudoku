@echo off
if NOT [%1] == [] (
if NOT [%2] == [] (
if NOT [%3] == [] (
if NOT [%4] == [] (
if NOT [%5] == [] (
  GOTO FEEDTOFILE
  ) ELSE ( GOTO GENERATOR) ) ELSE (GOTO MISSINGARG) ) ELSE (GOTO MISSINGARG) ) ELSE (GOTO MISSINGARG) ) ELSE (GOTO MISSINGARG)


:GENERATOR
((echo %1 %2 %3 %4) & (echo.)) > "temp"
bin\Generator.exe < "temp"
ERASE "temp"
GOTO END

:FEEDTOFILE
((echo %1 %2 %3 %4) & (echo.)) > "temp"
echo "Writing Puzzle to File: %5"
bin\Generator.exe < "temp" > %5
ERASE "temp"
GOTO END

:MISSINGARG
echo "Missing Argument! 'N p q M' are required!"
GOTO END
:END
