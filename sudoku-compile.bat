@echo off
cd src
cls
if /i "%~1" == "generator" (GOTO GENERATOR)
if /i "%~1" == "solver" (GOTO SOLVER)
if /i "%~1" == "all" (GOTO BOTH) ELSE (GOTO NONE)

:GENERATOR
    echo.
    echo "COMPILING GENERATOR"
    echo.
    ghc --make -o ..\bin\Generator.exe Generator
    GOTO ENDGENERATOR

:SOLVER
    echo.
    echo "COMPILING SOLVER"
    echo.
    ghc --make -o ..\bin\Solver.exe Main
    GOTO ENDSOLVER

:BOTH
    echo.
    echo "COMPILING SOLVER"
    echo.
    ghc --make -o ..\bin\Solver.exe Main.hs
    echo.
    echo "COMPILING GENERATOR"
    echo.
    ghc --make -o ..\bin\Generator.exe Generator.hs
    GOTO ENDALL

:NONE
    echo "NO MATCH TO ARGUMENT FOUND: generator,solver,all are only allowed choices"
    GOTO END

:ENDSOLVER
    IF EXIST "..\bin\Solver.exe" (
      echo "Solver Compilation Done."
      GOTO CLEAN
    ) ELSE (echo "Compilation error for Solver.exe")
    GOTO END

:ENDGENERATOR
  IF EXIST "..\bin\Generator.exe" (
    echo "Generator Compilation Done."
    GOTO CLEAN
  ) ELSE (echo "Compilation error for Generator.exe")
  GOTO END

:ENDALL
    IF EXIST "..\bin\Generator.exe" (
      IF EXIST "..\bin\Solver.exe" (
        echo "Compilation Solver + Generator Done."
        GOTO CLEAN
      ) ELSE (echo "Solver Compilation Encountered an Error.") 
    ) ELSE (echo "Generator Compilation Encountered an Error")

:CLEAN
ERASE /s *.o *.hi
:END
cd ../
