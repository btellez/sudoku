MONSTER SUDOKU SOLVER

Author: Bladymir Tellez <tellezb@uci.edu> #76123163
Partner: None

Language Requirements: Haskell GHC Compiler

Platform Requirements to use commands listed in this read-me: Windows
These methods were tested on the ICS Lab Machines.



Documentation
-------------------------------------------------------------------------------
Documentation can be found inside the `/docs` folder. This is documentation 
related to the code. Click the frames.html file or the index.html file to browse
the Java-Doc style documentation for the code.



Starting Out:
-------------------------------------------------------------------------------
Starting A Command Line Double Click the LAUNCH.bat file to open up a command 
line prompt at the 



Compiling:
-------------------------------------------------------------------------------
  Compile Generator and Solver:
    > sudoku-compile all

  Compile Generator Only
    > sudoku-compile generator

  Compile Solver Only
    > sudoku-compile solver



Generating A Puzzle:
-------------------------------------------------------------------------------
  Generating a puzzle requires inputs — N p q M [filepath]
  N, p, q, M are required to be Integer values.
  filepath is optional parameter, if it is provided then the generated puzzle 
  will be fed into that file, rather than output to the screen.

    *Without* a filepath parameter: 
    ...........................................................................
    > sudoku-generate N p q M
    (See bottom of file for piping generated directly into the solver.)

      | Example:
      | > sudoku-generate 12 3 4 20
      | 12 3 4
      | 0 0 0 0 0 0 0 0 0 0 0 0 
      | 0 6 0 0 0 0 C 0 0 0 0 0 
      | 0 0 0 0 0 0 0 0 0 0 0 0 
      | 0 0 0 0 0 0 0 9 0 0 0 0 
      | 0 0 0 0 0 1 3 0 0 0 0 0 
      | 0 0 0 0 0 0 0 0 0 0 0 0 
      | B 0 0 0 0 0 0 0 0 0 3 0 
      | 0 0 0 0 0 0 0 0 4 0 0 0 
      | 0 0 0 0 0 0 0 0 0 0 0 0 
      | 0 0 0 0 C 0 0 0 0 0 0 0 
      | 0 0 0 0 0 0 0 0 0 0 0 0 
      | C 0 0 0 0 0 0 0 0 0 0 0 
    
    *With* a filepath parameter
    ...........................................................................
    > sudoku-generate N p q M puzzle.txt

    puzzle.txt will be created in the current directory and will hold the 
    contents generated.



Solving A Puzzle:
-------------------------------------------------------------------------------
  Solving a puzzle has:
      1 required parameter, which is a filepath containing a puzzle.
      1 Optional Parameter, which is the method to use to solve the puzzle.
        Method Availeble:
        BT    -->  backtracking [Default -- if not specified]
        FC    -->  backtracking FC
        MRV   -->  backtracking FC + MRV
        DH    -->  backtracking FC + MRV + DH
        LCV   -->  backtracking FC + MRV + DH + LCV
        ACP   -->  backtracking FC + MRV + DH + LCV + AC as Preprocessing
        AC    -->  backtracking FC + MRV + DH + LCV + AC Preprocessing and at Every Step

        Example Usage:
        > sudoku-solver puzzle.txt
        > sudoku-solver puzzle.txt BT
        > sudoku-solver puzzle.txt FC
        > sudoku-solver puzzle.txt ACP



Other Miscellanious Operations:
-------------------------------------------------------------------------------
  Piping Generator output to Solver:

  > sudoku-generate 12 3 4 40 | bin\Solver.exe BT
  > sudoku-generate 9 3 3 10 | bin\Solver.exe BT
