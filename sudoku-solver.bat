@echo off
REM First - Make sure we have a file to get input from.
REM Then  - Make sure that that file exists.
REM Lastly - Determine what method of solving we will use.

if NOT [%1] == [] (
  if EXIST %1 (
    if NOT [%2] == [] (
      GOTO SOLVERARGS
    ) ELSE (GOTO SOLVER)
  ) ELSE (GOTO :MISSINGFILE) 
) ELSE (GOTO :MISSINGARG)


:SOLVER
bin\Solver.exe < %1
GOTO END

:SOLVERARGS
bin\Solver.exe %2 < %1
GOTO END

:MISSINGARG
echo "Missing Argument!"
GOTO END

:MISSINGFILE
echo "FILE NOT FOUND!"
GOTO END
:END
